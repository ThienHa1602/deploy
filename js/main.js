var dsSV = [];
const DSSV_LOCAL = "DSSV_LOCAL";
// khi user load trang => lấy dữ liệu từ localStorage
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  dsSV = JSON.parse(jsonData);
  renderDSSV(dsSV);
}

console.log("jsonData: ", jsonData);
function themSV() {
  var sv = layThongTinTuForm();
  // push(): thêm phần tử vào array
  dsSV.push(sv);

  // convert data
  var dataJson = JSON.stringify(dsSV);
  // lưu vào localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  // render danh sách sinh viên lên table
  // tbodySinhVien
  renderDSSV(dsSV);
  resetForm();
}
function xoaSV(id) {
  //splice: cut, slice: copy
  var viTri = -1;
  for (var i = 0; i < dsSV.length; i++) {
    if (dsSV[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // splice (vị trí, số lượng)
    dsSV.splice(viTri, 1);
    renderDSSV(dsSV);
  }
}
function suaSV(id) {
  console.log("id: ", id);
  var viTri = dsSV.findIndex(function (item) {
    return item.ma == id;
  });
  console.log("viTri: ", viTri);
  // show thông tin lên form
  var sv = dsSV[viTri];

  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function capNhatSV() {
  // layThongTinTuForm() => return object sv
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);
  var viTri = dsSV.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dsSV[viTri] = sv;
  renderDSSV(dsSV);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
// localStorage : lưu trữ, JSON : convert dât (chuyển đổi dữ liệu)
