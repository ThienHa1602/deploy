function renderDSSV(dsSV) {
  var contentHTML = "";
  for (var i = 0; i < dsSV.length; i++) {
    var item = dsSV[i];
    var contentTr = `
    <tr>
        <td> ${item.ma} </td>
        <td> ${item.ten} </td>
        <td> ${item.email} </td>
        <td> 0 </td>
        <td>
        <button onclick="xoaSV('${item.ma}')" class="btn btn-danger">
        Xóa
        </button>
        <button onclick="suaSV('${item.ma}')" class="btn btn-danger">
        Sửa
        </button>
        </td>
    </tr>
     `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  // lấy dữ liệu từ form
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matkhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  return {
    ma: ma,
    ten: ten,
    email: email,
    matkhau: matkhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
}
